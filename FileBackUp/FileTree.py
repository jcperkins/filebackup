# function copy_folder 
import os
import shutil

class FileTree (object):

    def __init__(self, source):
        self.source = os.path.normcase(source)
        self.file_count = -1
        self.transfer_count = -1
        self.tree_size = -1
        self.transfer_size = -1

    @staticmethod
    def copy_if_newer(src_path, dst_path, logging):
        if (os.path.exists(dst_path) and
                os.path.getmtime(src_path) <= os.path.getmtime(dst_path)):
            if logging == 'high':
                print("\tSkipping file %s" % os.path.basename(src_path))
            return False
        else:
            if logging == 'high':
                print("\tCopying file %s" % os.path.basename(src_path))
            try:
                shutil.copy2(src_path, dst_path)
            except PermissionError as err:
                print(err)
                return False
            return True

    def copy_tree(self,settings, destination, logging='low'):
        destination = os.path.normcase(os.path.abspath(destination))
        logging = logging.lower()
        copied_files = 0
        # get all folders and files from source
        for src_folder, subfolders, filenames in os.walk(self.source):
            if logging in ['high', 'medium']:
                print('Processing %s (%i subfolders and %i files)' % (
                    src_folder, len(subfolders), len(filenames)))
            dest_folder = os.path.normcase(
                src_folder).replace(self.source, destination, 1)
            if not os.path.exists(dest_folder):
                os.makedirs(dest_folder)

            for filename in filenames:
                result = self.copy_if_newer(os.path.join(src_folder, filename),
                                         os.path.join(dest_folder, filename),
                                         logging)
                if result:
                    copied_files += 1

        return copied_files

    def count_files(self):
        if self.file_count < 0:
            count = 0
            for folder, subfolders, filenames in os.walk(self.source):
                count += len(filenames)
            self.file_count = count

        return self.file_count

    def get_size(self):
        if self.tree_size < 0:
            size = 0
            for folder, subfolders, filenames in os.walk(self.source):
                for file in filenames:
                    size += os.stat(os.path.join(self.source, file)).st_size
            self.tree_size = size

        return self.tree_size
