# FileBackUp.py

# TODO: add ZipFile functionality: https://automatetheboringstuff.com/chapter9/
from FileBackUp import config
from FileBackUp import FileTree
verbose = 'medium'
# 1. read a list of directories to copy
# Main Script
settings = config.config()

# task based config files
#settings = config.config(r'./settings/MMI_Gen2QualPSFails_Transfers.json')
#settings = config.config(r'./settings/MMI_Gen2QualPSFails_Sort.json')
#settings = config.config(r'./settings/MMI_TH_Gen2QualPSFails_TransferAndSort.json')

transfers = settings.getTransferList()

# 2. loop through each transfer list
# for transfer in transfers:
for transfer in transfers:
    # recursively copy the contents of each folder
    if verbose is not 'silent':
        print('Processing file tree %s' % (transfer['source']))

    tree = FileTree.FileTree(transfer['source'])
    copies = tree.copy_tree(settings, transfer['destination'], verbose)
    #copy_folder(settings, transfer['source'], transfer['destination'], verbose)
    if verbose is not 'silent':
        print('Copied %i files from %s\n' % (copies, transfer['source']))
