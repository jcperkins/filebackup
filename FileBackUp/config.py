# config.py
import json

class config(object):
    """class will be initilized with a reference """
  
    def __init__(self, path=r'./settings/config.json'):
        self.path = path
        self.data = {}
        self.index = 0

    def read(self):
        with open(self.path) as config_file:
            self.data = json.load(config_file)
        return self.data

    def getTransferList(self):
        if(self.data == {}):
            self.read()
        return self.data["Directories"]



